# Enehano FE Test

Cílem zadaní je implementovat jednoduchou aplikaci pro správu kontaktů.

##### Aplikace by měla splňoat minimálně tyto požadavky:
  - Vytvoření nového kontaktu. Kontakt bude obsahovat jméno, příjmení, adresu, telefon, email, poznámku (až 4000 znaků).
  - Zobrazení vytvořených kontaktů.
  - Editaci vybraného kontaktu.
  - Smazaní vybraného kontaktu.
  
##### Funkcionalita, která je jako bonus:
 - Validace hodnot před uložením
 	- email je ve správném formátu
	- jméno, příjmení jsou povinná pole
	- je vypplněný email, nebo telefon (může být oboje, ale musí být alespoň jedno)
 - Tabulka se záznany bude podporovat stránkování, filtrování výsledků a řažení podle vybraného slopce (doporučuji použít nějakou externí knihovnu - např. [Data tables](https://datatables.net/))
 - Po refreshi stránky se nesmažou uložené záznamy (stačí využít local storage prohlížeče)
 
#### Pár tipů k hodnocení implementace
 - první kritérium se funkčnost.
 - jedná se o frontendovou komponentu - vyzuální stránka bude také hodnocena.
 - velký duraz bude kladen na čistotu kódu
 - v případě použití jakékoli externí knihovny, prosím, počitejet s tím, že knihovna musí být duď součástí odevzdané práce, nebo připojená takovým způsobem aby byla aplikace funkční (link dostupný z internetu, pomocí nmp managegeru atd.).
 
#### Odevzdání kódu
 - forkem si vytvořte vlastní repozitář
 - až budete mít hotovo stačí vytvořit pull request na repozitář se zadaním.