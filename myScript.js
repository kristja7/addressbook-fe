class State{
    constructor(initialContacts) {
        this._contacts = initialContacts; 
    }

    addContact = function (contact) {
        this._contacts.push(contact);
    }

    getNote = function(name, surname){
        for(var i = 0; i < this._contacts.length; i++){
            if(this._contacts[i].Name == name && this._contacts[i].Surname == surname){
                return this._contacts[i].Note;
            }
        }
        
    }

    
    getAllContacts = function () {
        return this._contacts;
    }

 
    removeContact = function (name, surname) {
        var removed = this._contacts.filter((contact) => {
            if(contact.Name == name && contact.Surname == surname){
                return false;
            }
            else{
                return true;
            }
        })
        this._contacts = removed;
        saveToStorage();
        setTimeout(createHtmlWithCreateElement,550, state.getAllContacts(), contactListEl)
    }
}

//variables
let modalVisible = false;
let noteVisible = false;
const contactListEl = document.getElementById("tbody");
const inputElName = document.getElementById("save");


//load contacts and put them into html
const state = new State([]);
loadFromStorage();
createHtmlWithCreateElement(state.getAllContacts(), contactListEl);


//event listeners
const modalButtons = document.querySelectorAll('.modal-button');
for (let i = 0; i < modalButtons.length; i++) {
        modalButtons[i].addEventListener('click', toggleModalState);
    }
document.getElementById("settings").addEventListener('click', toggleModalState);
document.getElementById("noteButton").addEventListener('click', toggleNoteState);
document.getElementById("save").addEventListener('click', validate);





//render contacts
function createHtmlWithCreateElement (contacts, targetEl) {
    $("#tbody tr").remove(); 
    for(const contact of contacts){

        // Prepare elements
        //row
        const contactEl = document.createElement('tr');

        //cells
        const nameEl = document.createElement('td');
        nameEl.textContent= contact.Name;
        const surnameEl = document.createElement('td');
        surnameEl.textContent= contact.Surname;
        const adressEl = document.createElement('td');
        adressEl.textContent= contact.Adress;
        const phoneEl = document.createElement('td');
        phoneEl.textContent= contact.Phone;
        const emailEl = document.createElement('td');
        emailEl.textContent= contact.Email;
        const noteButtonEl = document.createElement('button');
        noteButtonEl.textContent = "Poznámka";
        const noteEl = document.createElement('td');
        
        
        //edit and delete buttons
        const editButtonEl = document.createElement('button');
        editButtonEl.textContent = "Upravit";
        editButtonEl.classList.add('edit');
        const deleteButtonEl = document.createElement('button');
        deleteButtonEl.classList.add('destroy');
        deleteButtonEl.textContent = "X";
        const manageEl = document.createElement('td');
        

        contactEl.addEventListener('dblclick', edit);
        contactEl.appendChild(nameEl);
        contactEl.appendChild(surnameEl);
        contactEl.appendChild(adressEl);
        contactEl.appendChild(phoneEl);
        contactEl.appendChild(emailEl);
        contactEl.appendChild(noteEl);

        if(contact.Note != ""){
            noteEl.appendChild(noteButtonEl);
        }
        
        manageEl.appendChild(editButtonEl);
        manageEl.appendChild(deleteButtonEl);
        contactEl.appendChild(manageEl);
        editButtonEl.addEventListener('click', edit);
        deleteButtonEl.addEventListener('click', deleteContact);
        noteButtonEl.addEventListener('click', showNote);

        // Append contact to the page
        targetEl.appendChild(contactEl);

        
        
    }
}



//add contact menu visibility toggle
function toggleModalState () {
    modalVisible = !modalVisible;
    if (modalVisible) {
        document.body.classList.add('modal-visible');
    } else {
        document.body.classList.remove('modal-visible');
    }                
        
}


//note visibility toggle
function toggleNoteState(){
    noteVisible = !noteVisible;
    if (noteVisible) {
        document.body.classList.add('note-visible');
    } else {
        document.body.classList.remove('note-visible');
    }     
}


//get note into html
function showNote(e){
    var name = e.target.parentNode.parentNode.children[0].innerText;
    var surname = e.target.parentNode.parentNode.children[1].innerText;

    var note = state.getNote(name, surname);

    document.getElementById("note").innerHTML = note;
    toggleNoteState();
    
}



//Editing contact
function edit(e){
    toggleModalState();
    var name = e.target.parentNode.parentNode.children[0].innerText;
    var surname = e.target.parentNode.parentNode.children[1].innerText;

    //fill inputs
    for(var i = 0; i < e.target.parentNode.parentNode.children.length-2; i++){
        inputElName.parentElement.children[i].value =  e.target.parentNode.parentNode.children[i].innerText;
    }
    inputElName.parentElement.children[5].value = state.getNote(name, surname);

    state.removeContact(name, surname);
    document.getElementById("inputIdName").focus();
}



 
//Validate contact
function validate(e){
    //name and surname must be filled
    if(document.getElementById("inputIdName").value != '' && document.getElementById("inputIdSurname").value != '' ){
        //phone or email must be filled
        if(document.getElementById("inputIdPhone").value != '' || document.getElementById("inputIdEmail").value != ''){
            //email must be in correct format
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(document.getElementById("inputIdPhone").value != ''|| document.getElementById("inputIdEmail").value.match(mailformat)){
                addContact(e);
                return;
            }
            alert("Email není zadán v platném formátu");
            return;
        }
        alert("Není vyplněn telefon nebo email");
        return;
    }
    alert("Není vyplněno jméno nebo příjmení");
    
}

//Adding contact
function addContact(e){
    // Update app state
    const contact = {
        Name: document.getElementById("inputIdName").value,
        Surname: document.getElementById("inputIdSurname").value,
        Adress: document.getElementById("inputIdAdress").value,
        Phone: document.getElementById("inputIdPhone").value,
        Email: document.getElementById("inputIdEmail").value,
        Note: document.getElementById("inputIdNote").value
        
    };

    state.addContact(contact);

    // Update html
    for(var i = 0; i < inputElName.parentElement.children.length; i++){
        inputElName.parentElement.children[i].value = '';
    }

    inputElName.value = '';
    
    createHtmlWithCreateElement(state.getAllContacts(), contactListEl);
    saveToStorage();
    //close menu
    toggleModalState();
    location.reload();
}


//delete Contact
function deleteContact(e){
    var name = e.target.parentNode.parentNode.children[0].innerText;
    var surname = e.target.parentNode.parentNode.children[1].innerText;
    e.target.parentNode.classList.add("remContact");
    state.removeContact(name, surname);
}




//saving contacts to local storage
function saveToStorage(){
        var x = state.getAllContacts();
        var y = state.getNote("qw", "as");
        localStorage.setItem("note", JSON.stringify(x));
        
}

//loading contacts from local storage
function loadFromStorage() {
    var x = JSON.parse(localStorage.getItem("note"));
    if(x != null){

        x.forEach(note => {
        state.addContact(note);
    });
            }
    
}